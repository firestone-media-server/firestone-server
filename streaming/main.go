package streaming

import (
	"Server/database"
	"Server/models"
	"Server/utils/enums"
	"database/sql"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/stampede"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

const (
	FFMPEG_DIR = "ffmpeg"
	// we want a nice big cache, these files aren't to big, but they can take at most a second to generate, so caching
	// just UX nicer
	M3U8_CACHE_SIZE = 8192
	// the P of something in these files changing is incredibly low, give it a day
	M3U8_CACHE_TTL = 1 * 60 * 60 * 24
)

func Register(db *sqlx.DB) chi.Router {
	r := chi.NewRouter()

	// set up a cache for m3u8 files
	m3u8Cache := stampede.Handler(M3U8_CACHE_SIZE, M3U8_CACHE_TTL)

	// Eventually, we'll cache, for now, we'll generate on the fly. It can't take that long, right?
	r.With(m3u8Cache).HandleFunc("/m3u8/{library}/{item}.m3u8", func(w http.ResponseWriter, r *http.Request) {
		// look up the library first
		library, err := database.GetLibraryByID(db, graphql.ID(chi.URLParam(r, "library")))
		if err == sql.ErrNoRows {
			http.NotFound(w, r)
			return
		}
		if err != nil {
			logrus.Error(err)
			http.Error(w, err.Error(), 500)
			return
		}

		// check the library contains videos
		if !(library.BaseType == enums.BASE_TYPE_HOMEVID || library.BaseType == enums.BASE_TYPE_TVSHOW) {
			logrus.Error("invalid library basetype")
			http.Error(w, "invalid library basetype", 500)
			return
		}

		itemID := chi.URLParam(r, "item")

		// now the item, for its location
		var relativeLocation string

		switch library.BaseType {
		case enums.BASE_TYPE_HOMEVID:
			homevideo, err := database.GetHomeVideoByID(db, graphql.ID(itemID))
			if err != nil {
				logrus.Error(err)
				http.Error(w, err.Error(), 500)
				return
			}
			relativeLocation = homevideo.RelativeFileLocation
		case enums.BASE_TYPE_TVSHOW:
			episode, err := database.GetTVShowEpisodeByID(db, graphql.ID(itemID))
			if err != nil {
				logrus.Error(err)
				http.Error(w, err.Error(), 500)
				return
			}
			relativeLocation = episode.RelativeFileLocation
		}

		source := library.Path + "/" + relativeLocation

		m3u8, err := MakeM3u8(source, chi.URLParam(r, "library"), itemID)
		if err != nil {
			logrus.Error(err)
			http.Error(w, err.Error(), 500)
			return
		}

		w.Header().Set("Content-Type", "application/vnd.apple.mpegURL")
		w.Write([]byte(*m3u8))
	})

	// (probably going to be MKV for video and m4a for audio)
	r.HandleFunc("/content/{library}/{item}.*", func(w http.ResponseWriter, r *http.Request) {
		noPrefixURL := strings.TrimPrefix(r.URL.String(), "/media/content/")
		urlParts := strings.Split(noPrefixURL, "/")

		library, err := database.GetLibraryByID(db, graphql.ID(urlParts[0]))
		if err == sql.ErrNoRows {
			http.NotFound(w, r)
			return
		}
		if err != nil {
			logrus.Error(err)
			http.Error(w, err.Error(), 500)
			return
		}

		itemID := chi.URLParam(r, "item")

		directPlay := r.URL.Query().Get("directplay")
		isDirectPlay := directPlay == "true"

		switch library.BaseType {
		case enums.BASE_TYPE_SONG:
			{
				song, err := database.GetSongByID(db, graphql.ID(itemID))
				if err == sql.ErrNoRows {
					http.NotFound(w, r)
					return
				}
				if err != nil {
					logrus.Error(err)
					http.Error(w, err.Error(), 500)
					return
				}

				if isDirectPlay {
					source := library.Path + "/" + song.RelativeFileLocation
					err = DirectPlayItem(w, r, source)
					if err != nil {
						logrus.Error(err)
						http.Error(w, err.Error(), 500)
						return
					}

					return
				}

				StreamSong(w, r, song, library)
			}
		case enums.BASE_TYPE_TVSHOW, enums.BASE_TYPE_HOMEVID:
			{
				var relativeLocation string

				switch library.BaseType {
				case enums.BASE_TYPE_HOMEVID:
					homevideo, err := database.GetHomeVideoByID(db, graphql.ID(itemID))
					if err != nil {
						logrus.Error(err)
						http.Error(w, err.Error(), 500)
						return
					}
					relativeLocation = homevideo.RelativeFileLocation
				case enums.BASE_TYPE_TVSHOW:
					episode, err := database.GetTVShowEpisodeByID(db, graphql.ID(itemID))
					if err != nil {
						logrus.Error(err)
						http.Error(w, err.Error(), 500)
						return
					}
					relativeLocation = episode.RelativeFileLocation
				}

				source := library.Path + "/" + relativeLocation

				if isDirectPlay {
					err = DirectPlayItem(w, r, source)
					if err != nil {
						logrus.Error(err)
						http.Error(w, err.Error(), 500)
						return
					}

					return
				}

				err = StreamVideo(w, r, source)
				if err != nil {
					logrus.Error(err)
					http.Error(w, err.Error(), 500)
					return
				}
			}
		}
	})

	return r
}

func DirectPlayItem(w http.ResponseWriter, r *http.Request, source string) error {
	f, err := os.Open(source)
	if err != nil {
		return err
	}

	stats, err := f.Stat()
	if err != nil {
		return err
	}

	http.ServeContent(w, r, f.Name(), stats.ModTime(), f)

	return nil
}

func StreamSong(w http.ResponseWriter, r *http.Request, song *models.Song, library *models.Library) {
	fullPath := library.Path + "/" + song.RelativeFileLocation
	outLoc := os.TempDir() + "/" + string(song.ID) + ".m4a"

	if _, err := os.Stat(outLoc); os.IsNotExist(err) {
		var cmdLine []string
		if strings.HasSuffix(fullPath, ".m4a") {
			cmdLine = []string{
				"-i", fullPath,
				"-c:a", "copy",
				"-movflags", "+faststart",
				"-vn",
				outLoc,
			}
		} else {
			cmdLine = []string{
				"-i", fullPath,
				"-c:a", "aac",
				"-movflags", "+faststart",
				"-vn",
				outLoc,
			}
		}

		cmd := exec.Command(FFMPEG_DIR, cmdLine...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		err = cmd.Run()
		if err != nil {
			logrus.Error(err)
			http.Error(w, err.Error(), 500)
			return
		}
	}

	http.ServeFile(w, r, outLoc)
}

func StreamVideo(w http.ResponseWriter, r *http.Request, source string) error {
	start := "0.0"
	if s := r.URL.Query().Get("start"); s != "" {
		start = s
	}

	cmdLine := []string{
		"-hide_banner",

		"-ss", start,
		"-i", source,
		"-t", fmt.Sprintf("%.2f", SEGMENT_LENGTH),

		// x264 video codec
		"-vcodec", "libx264",
		// x264 preset
		"-preset", "veryfast",

		"-c:a", "aac",
		"-force_key_frames", fmt.Sprintf("expr:gte(t,n_forced*%.2f)", SEGMENT_LENGTH),
		"-f", "ssegment",
		"-segment_time", fmt.Sprintf("%.2f", SEGMENT_LENGTH),
		"-initial_offset", start,

		"pipe:out%03d.ts",
	}

	cmd := exec.Command("ffmpeg", cmdLine...)

	stdout, err := cmd.StdoutPipe()

	if err != nil {
		return err
	}

	err = cmd.Start()
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "video/m2ts")
	_, err = io.Copy(w, stdout)
	if err != nil {
		return err
	}

	return nil
}
