package streaming

import (
	"Server/metadata"
	"fmt"
	"math"
)

const SEGMENT_LENGTH float64 = 6.0

var M3U8_START = fmt.Sprintf(`#EXTM3U
#EXT-X-TARGETDURATION:%.2f
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
`, SEGMENT_LENGTH)

func MakeM3u8(source string, libraryID string, itemID string) (*string, error) {
	length, err := metadata.GetFileLengthUnrounded(source)
	if err != nil {
		return nil, err
	}

	var m3u8 string

	segments := math.Round(*length / 6)

	m3u8 += M3U8_START

	m3u8 += fmt.Sprintf(`#EXTINF:%.2f
/media/content/%s/%s.ts?end=6
`, SEGMENT_LENGTH, libraryID, itemID)

	for i := 1.0; i < segments; i++ {
		m3u8 += fmt.Sprintf(`#EXTINF:%.2f
/media/content/%s/%s.ts?start=%.0f&end=%.0f
`, SEGMENT_LENGTH, libraryID, itemID, i*SEGMENT_LENGTH, i*SEGMENT_LENGTH+SEGMENT_LENGTH)
	}

	m3u8 += fmt.Sprintf(`#EXTINF:%.2f
"/media/content/%s/%s.ts?start=%.0f
`, (*length)-segments*SEGMENT_LENGTH, libraryID, itemID, segments*SEGMENT_LENGTH)

	m3u8 += "\n#EXT-X-ENDLIST"

	return &m3u8, nil
}
