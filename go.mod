module Server

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dhowden/tag v0.0.0-20191122115059-7e5c04feccd8
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-chi/stampede v0.4.4
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/graph-gophers/graphql-go v0.0.0-20200207002730-8334863f2c8b
	github.com/jmoiron/sqlx v1.2.0
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/michiwend/golang-pretty v0.0.0-20141116172505-8ac61812ea3f // indirect
	github.com/michiwend/gomusicbrainz v0.0.0-20181012083520-6c07e13dd396
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	google.golang.org/appengine v1.6.6 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
