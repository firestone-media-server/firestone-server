package main

import (
	"Server/utils"
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
	"time"
)

func SetupAuthnRouter() http.Handler {
	r := chi.NewRouter()
	r.Post("/login", LoginFormHandler)
	// eventually there will be a get here too.
	r.With(SessionMiddleware(false)).Post("/newacc", UserCreateHandle)

	return r
}

func LoginFormHandler(w http.ResponseWriter, r *http.Request) {
	if _, _, err := jwtauth.FromContext(r.Context()); err == nil {
		return
	}

	// just allow the request to continue

	username := r.FormValue("username")
	password := r.FormValue("password")

	if username == "" || password == "" {
		http.Error(w, "No username or password passed", http.StatusBadRequest)
		return
	}

	db := r.Context().Value("db").(*sqlx.DB)

	user := &utils.User{}
	err := db.Get(user, "SELECT * FROM user WHERE userName=?", username)
	if err != nil {
		logrus.Error(err)
		http.Error(w, "Not Found", http.StatusForbidden)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		http.Error(w, "Not Found", http.StatusForbidden)
		return
	}

	tokenAuth := r.Context().Value("tokenAuth").(*jwtauth.JWTAuth)
	_, tokenString, _ := tokenAuth.Encode(jwt.MapClaims{"user_id": user.UserID})

	w.Header().Add("X-Set-Token", tokenString)
	w.Header().Add("Set-Cookie", fmt.Sprintf("jwt=%s; HttpOnly; Max-Age=%d; Path=/", tokenString, SESSION_TIME))
}

func UserCreateHandle(w http.ResponseWriter, r *http.Request) {
	if user := r.Context().Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		http.Error(w, "non admin users cannot add new users", http.StatusUnauthorized)
		return
	}

	username := r.FormValue("username")
	password := r.FormValue("password")
	isAdminString := r.FormValue("isAdmin")

	if isAdminString == "" {
		isAdminString = "false"
	}

	if username == "" || password == "" {
		http.Error(w, "No username or password passed", http.StatusBadRequest)
		return
	}

	isAdmin, err := strconv.ParseBool(isAdminString)
	if err != nil {
		logrus.Error(err)
		http.Error(w, "failed to create account", http.StatusInternalServerError)
		return
	}

	db := r.Context().Value("db").(*sqlx.DB)

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		logrus.Error(err)
		http.Error(w, "failed to create account", http.StatusInternalServerError)
		return
	}

	id := uuid.NewV4()
	_, err = db.Exec("INSERT INTO User (id, userName, password, isAdmin, createdAt) VALUES (?,?,?,?,?)",
		id, username, passwordHash, isAdmin, time.Now().Unix())
	if err != nil {
		logrus.Error(err)
		http.Error(w, "failed to create account", http.StatusInternalServerError)
		return
	}

	w.Write([]byte(id.String()))
	w.WriteHeader(http.StatusCreated)
}

func SessionMiddleware(mustHaveSession bool) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var user *utils.User = nil

			token, claims, err := jwtauth.FromContext(r.Context())
			if mustHaveSession {
				if err != nil {
					http.Error(w, http.StatusText(401), 401)
					return
				}
				if token == nil || !token.Valid {
					http.Error(w, http.StatusText(401), 401)
					return
				}

				userID := claims["user_id"]
				user = GetSessionUser(userID.(string), r.Context().Value("db").(*sqlx.DB))
			}

			ctx := context.WithValue(r.Context(), "user", user)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func GetSessionUser(userID string, db *sqlx.DB) *utils.User {
	u := &utils.User{}
	db.Get(u, "SELECT id, userName, isAdmin, createdAt FROM user WHERE id=?", userID)

	return u
}
