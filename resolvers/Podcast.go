package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/utils"
	"Server/utils/enums"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

// Podcast

type PodcastResolver struct {
	db      *sqlx.DB
	podcast *models.Podcast
}

func NewPodcastResolver(db *sqlx.DB, podcast *models.Podcast) *PodcastResolver {
	return &PodcastResolver{
		db,
		podcast,
	}
}

func (r *PodcastResolver) ID() graphql.ID {
	return r.podcast.ID
}

func (r *PodcastResolver) Name() string {
	return r.podcast.Name
}

func (r *PodcastResolver) Items() ([]*PodcastEpisodeResolver, error) {
	var podcastEpisodes []*models.PodcastEpisode

	err := r.db.Select(&podcastEpisodes, "SELECT * FROM PodcastEpisode WHERE podcastID=?", r.podcast.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	var resolvers []*PodcastEpisodeResolver
	for _, episode := range podcastEpisodes {
		resolvers = append(resolvers, NewPodcastEpisodeResolver(r.db, episode))
	}

	return resolvers, nil
}

func (r *PodcastResolver) RSSFeedURL() *string {
	return r.podcast.RSSFeedURL
}

func (r *PodcastResolver) With() ([]*models.Person, error) {
	var people []*models.Person
	err := r.db.Select(&people, "SELECT P.id, P.name, P.born, P.died, P.createdAt FROM Person P JOIN podcast_people pp on P.id = pp.personID WHERE pp.podcastID=?", r.podcast.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return people, nil
}

func (r *PodcastResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(r.podcast.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

// Podcast Episode

type PodcastEpisodeResolver struct {
	db      *sqlx.DB
	podcast *models.PodcastEpisode
}

func NewPodcastEpisodeResolver(DB *sqlx.DB, podcast *models.PodcastEpisode) *PodcastEpisodeResolver {
	return &PodcastEpisodeResolver{
		db:      DB,
		podcast: podcast,
	}
}

func (pr *PodcastEpisodeResolver) ID() graphql.ID {
	return pr.podcast.ID
}

func (pr *PodcastEpisodeResolver) Name() string {
	return pr.podcast.Name
}

func (pr *PodcastEpisodeResolver) BaseType() string {
	return pr.podcast.BaseType
}

func (pr *PodcastEpisodeResolver) FileType() string {
	return pr.podcast.FileType
}

func (pr *PodcastEpisodeResolver) Podcast() (*PodcastResolver, error) {
	podcast, err := database.GetPodcastByID(pr.db, pr.podcast.ID)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewPodcastResolver(pr.db, podcast), nil
}

func (pr *PodcastEpisodeResolver) EpisodeNumber() int32 {
	return pr.podcast.EpisodeNumber
}

func (pr *PodcastEpisodeResolver) SeasonNumber() int32 {
	return pr.podcast.SeasonNumber
}

func (pr *PodcastEpisodeResolver) Length() float64 {
	return pr.podcast.Length
}

func (pr *PodcastEpisodeResolver) RelativeFileLocation() string {
	return pr.podcast.RelativeFileLocation
}

func (pr *PodcastEpisodeResolver) Library() (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(pr.db, graphql.ID(pr.podcast.LibraryID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(pr.db, library), nil
}

func (pr *PodcastEpisodeResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(pr.podcast.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

func (pr *PodcastEpisodeResolver) ReleaseDate() *graphql.Time {
	if pr.podcast.DBReleasedAt == nil {
		return nil
	}
	releasedAtTime := time.Unix(int64(*pr.podcast.DBReleasedAt), 0)
	return &graphql.Time{Time: releasedAtTime}
}

// Root

func (r *Root) Podcast(args struct{ ID graphql.ID }) (*PodcastResolver, error) {
	podcast, err := database.GetPodcastByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewPodcastResolver(r.DB, podcast), nil
}

func (r *Root) Podcasts(args struct{ Page *Paging }) ([]*PodcastResolver, error) {
	var (
		podcasts               = []*models.Podcast{}
		podcastResolvers       = []*PodcastResolver{}
		start            int32 = 0
		count            int32 = DEFAULT_COUNT
	)

	err := r.DB.Select(&podcasts, "SELECT * FROM Podcast")

	if err == sql.ErrNoRows {
		return podcastResolvers, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}

	if start > int32(len(podcasts)) {
		return podcastResolvers, nil
	}

	if start+count > int32(len(podcasts)) {
		count = int32(len(podcasts)) - start
	}

	podcasts = podcasts[start : start+count]

	for _, podcast := range podcasts {
		podcastResolvers = append(podcastResolvers, NewPodcastResolver(r.DB, podcast))
	}

	return podcastResolvers, nil
}

type AddPodcast struct {
	Name       string
	Library    graphql.ID
	With       []graphql.ID
	RSSFeedURL *string
}

func (r *Root) AddPodcast(ctx context.Context, args struct{ Podcast AddPodcast }) (*PodcastResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new podcasts")
	}

	id := uuid.NewV4().String()

	pod := args.Podcast

	_, err := r.DB.Exec("INSERT INTO Podcast (id, name, rssFeedURL, libraryID, createdAt) VALUES (?,?,?,?,?)",
		id, pod.Name, pod.RSSFeedURL, pod.Library, time.Now().Unix())

	if err != nil {
		return nil, err
	}

	for _, person := range args.Podcast.With {
		_, err := r.LinkPodcastToPerson(ctx, LinkPodToPersonArgs{
			Podcast: graphql.ID(id),
			Person:  person,
		})
		if err != nil {
			return nil, err
		}
	}

	podcast, err := database.GetPodcastByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewPodcastResolver(r.DB, podcast), nil
}

// PodcastEpisode

func (r *Root) PodcastEpisode(args struct{ ID graphql.ID }) (*PodcastEpisodeResolver, error) {
	podcast, err := database.GetPodcastEpisodeByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewPodcastEpisodeResolver(r.DB, podcast), nil
}

type AddPodcastEpisode struct {
	Name                 string
	RelativeFileLocation string
	FileType             string
	Length               float64
	Library              graphql.ID
	ReleaseDate          *graphql.Time
	Podcast              graphql.ID
	EpisodeNumber        *int32
	SeasonNumber         *int32
}

func (r *Root) AddPodcastEpisode(ctx context.Context, args struct{ Podcast AddPodcastEpisode }) (*PodcastEpisodeResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new podcast episodes")
	}

	id := uuid.NewV4().String()

	var releaseDate *int64 = nil

	if args.Podcast.ReleaseDate != nil {
		nixtime := args.Podcast.ReleaseDate.Unix()
		releaseDate = &nixtime
	}

	if args.Podcast.EpisodeNumber == nil {
		*args.Podcast.EpisodeNumber = int32(0)
	}

	pod := args.Podcast

	_, err := r.DB.Exec(`INSERT INTO PodcastEpisode
    	(id, name, baseType, relativeFileLocation, fileType, length, episodeNumber, seasonNumber, libraryID, createdAt, releaseDate, podcastID)
    	VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`,
		id, pod.Name, enums.BASE_TYPE_PODCAST, pod.RelativeFileLocation, pod.FileType, pod.Length, pod.EpisodeNumber, pod.SeasonNumber,
		string(pod.Library), time.Now().Unix(), releaseDate, pod.Podcast)

	if err != nil {
		return nil, err
	}

	podcast, err := database.GetPodcastEpisodeByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewPodcastEpisodeResolver(r.DB, podcast), nil
}

func (r *Root) DeletePodcastEpisode(ctx context.Context, args struct{ ID graphql.ID }) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete podcast episodes")
	}

	_, err := r.DB.Exec("DELETE FROM PodcastEpisode WHERE id=?", args.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}
