package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/utils"
	"Server/utils/enums"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

type TVShowResolver struct {
	db   *sqlx.DB
	show *models.TVShow
}

func NewTVShowResolver(DB *sqlx.DB, tvShow *models.TVShow) *TVShowResolver {
	return &TVShowResolver{
		db:   DB,
		show: tvShow,
	}
}

func (r *TVShowResolver) ID() graphql.ID {
	return r.show.ID
}

func (r *TVShowResolver) Name() string {
	return r.show.Name
}
func (r *TVShowResolver) Library() (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(r.db, graphql.ID(r.show.LibraryID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(r.db, library), nil
}

func (r *TVShowResolver) Items() ([]*TVShowEpisodeResolver, error) {
	var tvShows = []*models.TVShowEpisode{}

	err := r.db.Select(&tvShows, "SELECT * FROM TVShowEpisode WHERE tvShowID=?", r.show.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	var resolvers []*TVShowEpisodeResolver
	for _, tvShow := range tvShows {
		resolvers = append(resolvers, NewTVShowEpResolver(r.db, tvShow))
	}

	return resolvers, nil
}

func (r *TVShowResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(r.show.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

// Episode

type TVShowEpisodeResolver struct {
	db      *sqlx.DB
	episode *models.TVShowEpisode
}

func NewTVShowEpResolver(db *sqlx.DB, episode *models.TVShowEpisode) *TVShowEpisodeResolver {
	return &TVShowEpisodeResolver{
		db,
		episode,
	}
}

func (r *TVShowEpisodeResolver) ID() graphql.ID {
	return r.episode.ID
}

func (r *TVShowEpisodeResolver) Name() string {
	return r.episode.Name
}

func (r *TVShowEpisodeResolver) BaseType() string {
	return r.episode.BaseType
}

func (r *TVShowEpisodeResolver) FileType() string {
	return r.episode.FileType
}

func (r *TVShowEpisodeResolver) EpisodeNumber() int32 {
	return r.episode.EpisodeNumber
}

func (r *TVShowEpisodeResolver) SeasonNumber() int32 {
	return r.episode.SeasonNumber
}

func (r *TVShowEpisodeResolver) Length() float64 {
	return r.episode.Length
}

func (r *TVShowEpisodeResolver) RelativeFileLocation() string {
	return r.episode.RelativeFileLocation
}

func (r *TVShowEpisodeResolver) Library() (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(r.db, graphql.ID(r.episode.LibraryID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(r.db, library), nil
}

func (r *TVShowEpisodeResolver) TVShow() (*TVShowResolver, error) {
	show, err := database.GetTVShowByID(r.db, graphql.ID(r.episode.TVShowID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewTVShowResolver(r.db, show), nil
}

func (r *TVShowEpisodeResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(r.episode.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

func (r *TVShowEpisodeResolver) ReleaseDate() *graphql.Time {
	if r.episode.DBReleasedAt == nil {
		return nil
	}
	releasedAtTime := time.Unix(int64(*r.episode.DBReleasedAt), 0)
	return &graphql.Time{Time: releasedAtTime}
}

// Root

func (r *Root) TVShow(args struct{ ID graphql.ID }) (*TVShowResolver, error) {
	tvShow, err := database.GetTVShowByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewTVShowResolver(r.DB, tvShow), nil
}

type AddTVShow struct {
	Name    string
	Library graphql.ID
}

func (r *Root) AddTVShow(ctx context.Context, args struct{ Show AddTVShow }) (*TVShowResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new TV shows")
	}

	id := uuid.NewV4().String()

	show := args.Show

	_, err := r.DB.Exec(`INSERT INTO
    							TVShow (id, name, libraryID, createdAt)
    							VALUES (?,?,?,?)`,
		id, show.Name, show.Library, time.Now().Unix())

	if err != nil {
		return nil, err
	}

	tvShow, err := database.GetTVShowByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewTVShowResolver(r.DB, tvShow), nil
}

// Root TVShowEpisode

func (r *Root) TVShowEpisode(args struct{ ID graphql.ID }) (*TVShowEpisodeResolver, error) {
	ep, err := database.GetTVShowEpisodeByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewTVShowEpResolver(r.DB, ep), nil
}

type AddTVShowEpisode struct {
	Name                 string
	RelativeFileLocation string
	FileType             string
	Length               float64
	Library              graphql.ID
	TVShow               graphql.ID
	ReleaseDate          *graphql.Time
	EpisodeNumber        int32
	SeasonNumber         int32
}

func (r *Root) AddTVShowEpisode(ctx context.Context, args struct{ Episode AddTVShowEpisode }) (*TVShowEpisodeResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new TV episodes")
	}

	id := uuid.NewV4().String()

	var releaseDate *int64 = nil

	if args.Episode.ReleaseDate != nil {
		nixtime := args.Episode.ReleaseDate.Unix()
		releaseDate = &nixtime
	}

	ep := args.Episode

	_, err := r.DB.Exec(`INSERT INTO
    							TVShowEpisode (id, name, baseType, relativeFileLocation, fileType, length, episodeNumber, seasonNumber, releaseDate, libraryID, tvShowID, createdAt)
    							VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`,
		id, ep.Name, enums.BASE_TYPE_TVSHOW, ep.RelativeFileLocation, ep.FileType, ep.Length, ep.EpisodeNumber,
		ep.SeasonNumber, releaseDate, ep.Library, ep.TVShow, time.Now().Unix())

	if err != nil {
		return nil, err
	}

	episode, err := database.GetTVShowEpisodeByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewTVShowEpResolver(r.DB, episode), nil
}

func (r *Root) DeleteTVShowEpisode(ctx context.Context, args struct{ ID graphql.ID }) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete TV show episodes")
	}

	_, err := r.DB.Exec("DELETE FROM TVShowEpisode WHERE id=?", args.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}
