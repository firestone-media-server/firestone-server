package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/scanner"
	"Server/utils"
	"Server/utils/enums"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

type LibraryResolver struct {
	library *models.Library
	db      *sqlx.DB
}

func NewLibraryResolver(db *sqlx.DB, l *models.Library) *LibraryResolver {
	return &LibraryResolver{
		library: l,
		db:      db,
	}
}

func (l *LibraryResolver) ID() graphql.ID {
	return l.library.ID
}

func (l *LibraryResolver) Name() string {
	return l.library.Name
}

func (l *LibraryResolver) Path() string {
	return l.library.Path
}

func (l *LibraryResolver) BaseType() string {
	return l.library.BaseType
}

func (l *LibraryResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(l.library.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

func (l *LibraryResolver) Items() ([]*MediaItemBaseResolver, error) {
	var items = []*MediaItemBaseResolver{}
	var err error

	switch l.library.BaseType {
	case enums.BASE_TYPE_SONG:
		err = l.db.Select(&items, "SELECT * FROM Song WHERE libraryID=?", l.library.ID)
	case enums.BASE_TYPE_IMAGE:
		err = l.db.Select(&items, "SELECT * FROM Images WHERE libraryID=?", l.library.ID)
	case enums.BASE_TYPE_HOMEVID:
		err = l.db.Select(&items, "SELECT * FROM HomeVideo WHERE libraryID=?", l.library.ID)
	case enums.BASE_TYPE_TVSHOW:
		err = l.db.Select(&items, "SELECT * FROM TVShowEpisode WHERE libraryID=?", l.library.ID)
	case enums.BASE_TYPE_PODCAST:
		err = l.db.Select(&items, "SELECT * FROM PodcastEpisode WHERE libraryID=?", l.library.ID)
	default:
		err = errors.New("invalid BaseType ¯\\_(ツ)_/¯")
	}

	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	for _, v := range items {
		v.DB = l.db
	}

	return items, nil
}

// Root

func (r *Root) Library(args struct{ ID graphql.ID }) (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(r.DB, library), nil
}

func (r *Root) Libraries(args struct{ Page *Paging }) ([]*LibraryResolver, error) {
	var (
		libraries              = []*models.Library{}
		libraryResolvers       = []*LibraryResolver{}
		start            int32 = 0
		count            int32 = DEFAULT_COUNT
	)

	err := r.DB.Select(&libraries, "SELECT * FROM Library")

	if err == sql.ErrNoRows {
		return libraryResolvers, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}
	if start > int32(len(libraries)) {
		return libraryResolvers, nil
	}

	if start+count > int32(len(libraries)) {
		count = int32(len(libraries)) - start
	}

	libraries = libraries[start : start+count]

	for _, library := range libraries {
		libraryResolvers = append(libraryResolvers, NewLibraryResolver(r.DB, library))
	}

	return libraryResolvers, nil
}

type InputAddLibrary struct {
	Name     string
	Path     string
	BaseType string
}

func (r *Root) AddLibrary(ctx context.Context, args struct{ Library InputAddLibrary }) (*LibraryResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot make new libraries")
	}

	// do we have a trailing /? if so, snip it off
	if args.Library.Path[len(args.Library.Path)-1] == '/' {
		args.Library.Path = args.Library.Path[:len(args.Library.Path)-1]
	}

	id := uuid.NewV4().String()

	_, err := r.DB.Exec("INSERT INTO Library (id, name, baseMediaType, path, createdAt) VALUES (?,?,?,?,?)",
		id, args.Library.Name, args.Library.BaseType, args.Library.Path, time.Now().Unix())

	if err != nil {
		return nil, err
	}

	library, err := database.GetLibraryByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	//Everything is fine, call the worker.
	r.Dispatcher.Add(func(ctx context.Context) {
		data := map[string]string{
			"path":     args.Library.Path,
			"baseType": args.Library.BaseType,
			"id":       id,
		}

		scanner.ScanDir(data, ctx)
	})

	return NewLibraryResolver(r.DB, library), nil
}

func (r *Root) DeleteLibrary(ctx context.Context, args CSCDeleteArgs) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete libraries")
	}

	return false, errors.New("not implemented")
}
