package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/utils"
	"Server/utils/enums"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

type HomeVideoResolver struct {
	db    *sqlx.DB
	video *models.HomeVideo
}

func NewHomeVideoResolver(db *sqlx.DB, video *models.HomeVideo) *HomeVideoResolver {
	return &HomeVideoResolver{
		db,
		video,
	}
}

func (r *HomeVideoResolver) ID() graphql.ID {
	return r.video.ID
}

func (r *HomeVideoResolver) Name() string {
	return r.video.Name
}

func (r *HomeVideoResolver) BaseType() string {
	return r.video.BaseType
}

func (r *HomeVideoResolver) FileType() string {
	return r.video.FileType
}

func (r *HomeVideoResolver) Length() float64 {
	return r.video.Length
}

func (r *HomeVideoResolver) RelativeFileLocation() string {
	return r.video.RelativeFileLocation
}

func (r *HomeVideoResolver) Library() (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(r.db, graphql.ID(r.video.LibraryID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(r.db, library), nil
}

func (r *HomeVideoResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(r.video.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

func (r *HomeVideoResolver) DateTaken() *graphql.Time {
	if r.video.DBDateTaken == nil {
		return nil
	}
	releasedAtTime := time.Unix(int64(*r.video.DBDateTaken), 0)
	return &graphql.Time{Time: releasedAtTime}
}

// Root

func (r *Root) HomeVideo(args struct{ ID graphql.ID }) (*HomeVideoResolver, error) {
	video, err := database.GetHomeVideoByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewHomeVideoResolver(r.DB, video), nil
}

func (r *Root) HomeVideos(args struct{ Page *Paging }) ([]*HomeVideoResolver, error) {
	var (
		homeVideos               = []*models.HomeVideo{}
		homeVideoResolvers       = []*HomeVideoResolver{}
		start              int32 = 0
		count              int32 = DEFAULT_COUNT
	)

	err := r.DB.Select(&homeVideos, "SELECT * FROM HomeVideo")

	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}

	if start > int32(len(homeVideos)) {
		return homeVideoResolvers, nil
	}

	if start+count > int32(len(homeVideos)) {
		count = int32(len(homeVideos)) - start
	}

	homeVideos = homeVideos[start : start+count]

	for _, homeVideo := range homeVideos {
		homeVideoResolvers = append(homeVideoResolvers, NewHomeVideoResolver(r.DB, homeVideo))
	}

	return homeVideoResolvers, nil
}

type AddHomeVideo struct {
	Name                 string
	RelativeFileLocation string
	FileType             string
	Library              graphql.ID
	Length               float64
	DateTaken            *graphql.Time
}

func (r *Root) AddHomeVideo(ctx context.Context, args struct{ Video AddHomeVideo }) (*HomeVideoResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new videos")
	}

	id := uuid.NewV4().String()

	var dateTaken *int64 = nil

	if args.Video.DateTaken != nil {
		nixtime := args.Video.DateTaken.Unix()
		dateTaken = &nixtime
	}

	vid := args.Video

	_, err := r.DB.Exec(`INSERT INTO
    							HomeVideo (id, name, baseType, relativeFileLocation, fileType, length, dateTaken, libraryID, createdAt)
    							VALUES (?,?,?,?,?,?,?,?,?)`,
		id, vid.Name, enums.BASE_TYPE_HOMEVID, vid.RelativeFileLocation, vid.FileType, vid.Length, dateTaken, vid.Library, time.Now().Unix())

	if err != nil {
		return nil, err
	}

	video, err := database.GetHomeVideoByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewHomeVideoResolver(r.DB, video), nil
}

func (r *Root) DeleteHomeVideo(ctx context.Context, args struct{ ID graphql.ID }) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete videos")
	}

	_, err := r.DB.Exec("DELETE FROM HomeVideo WHERE id=?", args.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}
