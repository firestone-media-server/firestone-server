package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/utils"
	"Server/utils/enums"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

type SongResolver struct {
	db   *sqlx.DB
	song *models.Song
}

func NewSongResolver(DB *sqlx.DB, song *models.Song) *SongResolver {
	return &SongResolver{
		db:   DB,
		song: song,
	}
}

func (sr *SongResolver) ID() graphql.ID {
	return sr.song.ID
}

func (sr *SongResolver) Name() string {
	return sr.song.Name
}

func (sr *SongResolver) BaseType() string {
	return sr.song.BaseType
}

func (sr *SongResolver) FileType() string {
	return sr.song.FileType
}

func (sr *SongResolver) Length() float64 {
	return sr.song.Length
}

func (sr *SongResolver) TrackNumber() int32 {
	return sr.song.TrackNumber
}

func (sr *SongResolver) DiscNumber() int32 {
	return sr.song.DiscNumber
}

func (sr *SongResolver) RelativeFileLocation() string {
	return sr.song.RelativeFileLocation
}

func (sr *SongResolver) Library() (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(sr.db, graphql.ID(sr.song.LibraryID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(sr.db, library), nil
}

func (sr *SongResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(sr.song.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

func (sr *SongResolver) Album() (*AlbumResolver, error) {
	album, err := database.GetAlbumByID(sr.db, graphql.ID(sr.song.AlbumID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewAlbumResolver(sr.db, album), nil
}

func (sr *SongResolver) Artist() (*ArtistResolver, error) {
	artist := models.Artist{}

	err := sr.db.Get(&artist, "SELECT * FROM Artist WHERE id=?", sr.song.Artist)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewArtistResolver(sr.db, &artist), nil
}

// Album

type AlbumResolver struct {
	db    *sqlx.DB
	album *models.Album
}

func NewAlbumResolver(db *sqlx.DB, album *models.Album) *AlbumResolver {
	return &AlbumResolver{
		db:    db,
		album: album,
	}
}

func (ar *AlbumResolver) ID() graphql.ID {
	return ar.album.ID
}

func (ar *AlbumResolver) Name() string {
	return ar.album.Name
}

func (ar *AlbumResolver) Compilation() bool {
	return ar.album.Compilation
}

func (ar *AlbumResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(ar.album.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

func (ar *AlbumResolver) ReleaseDate() *graphql.Time {
	if ar.album.ReleaseDate == nil {
		return nil
	}
	releaseDate := time.Unix(int64(*ar.album.ReleaseDate), 0)
	return &graphql.Time{Time: releaseDate}
}

func (ar *AlbumResolver) Artist() (*ArtistResolver, error) {
	artist := models.Artist{}

	err := ar.db.Get(&artist, "SELECT * FROM Artist WHERE id=?", ar.album.ArtistID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewArtistResolver(ar.db, &artist), nil
}

func (ar *AlbumResolver) ArtURL() *string {
	return ar.album.ArtURL
}

func (ar *AlbumResolver) Items() ([]*SongResolver, error) {
	var songs = []*models.Song{}

	err := ar.db.Select(&songs, "SELECT * FROM Song WHERE albumID=?", ar.album.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	var resolvers []*SongResolver
	for _, song := range songs {
		resolvers = append(resolvers, NewSongResolver(ar.db, song))
	}

	return resolvers, nil
}

// Artist

type ArtistResolver struct {
	db     *sqlx.DB
	artist *models.Artist
}

func NewArtistResolver(db *sqlx.DB, artist *models.Artist) *ArtistResolver {
	return &ArtistResolver{
		db:     db,
		artist: artist,
	}
}

func (ar *ArtistResolver) ID() graphql.ID {
	return ar.artist.ID
}

func (ar *ArtistResolver) Name() string {
	return ar.artist.Name
}

func (ar *ArtistResolver) Person() (*models.Person, error) {
	// Sanity check
	if ar.artist.IsGroup {
		return nil, nil
	}

	return database.GetPersonByID(ar.db, *ar.artist.Person)
}

func (ar *ArtistResolver) Members() (*[]*models.Person, error) {
	var people []*models.Person
	err := ar.db.Select(&people, "SELECT p.id, name, born, died, createdAt FROM artist_person JOIN Person p ON person=p.id WHERE artist=?", ar.artist.Person)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &people, nil
}

func (ar *ArtistResolver) Albums() ([]*AlbumResolver, error) {
	var (
		albums         = []*models.Album{}
		albumResolvers = []*AlbumResolver{}
	)
	err := ar.db.Select(&albums, "SELECT * FROM Albums WHERE artist=?", ar.artist.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	for _, album := range albums {
		albumResolvers = append(albumResolvers, NewAlbumResolver(ar.db, album))
	}

	return albumResolvers, nil
}

func (ar *ArtistResolver) IsGroup() bool {
	return ar.artist.IsGroup
}

func (ar *ArtistResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(ar.artist.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

// Root

func (r *Root) Song(args struct{ ID graphql.ID }) (*SongResolver, error) {
	song, err := database.GetSongByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewSongResolver(r.DB, song), nil
}

type QuerySongsArgs struct {
	Page    *Paging
	Library *graphql.ID
}

func (r *Root) Songs(args QuerySongsArgs) ([]*SongResolver, error) {
	var (
		start         int32 = 0
		count         int32 = DEFAULT_COUNT
		songs               = []*models.Song{}
		songResolvers       = []*SongResolver{}
	)

	var err error
	if args.Library != nil {
		err = r.DB.Select(&songs, "SELECT id, name, baseType, relativeFileLocation, trackNumber, discNumber, length, fileType, libraryID, albumID, createdAt, artist FROM Song WHERE libraryID=?", *args.Library)
	} else {
		err = r.DB.Select(&songs, "SELECT id, name, baseType, relativeFileLocation, trackNumber, discNumber, length, fileType, libraryID, albumID, createdAt, artist FROM Song")
	}

	if err == sql.ErrNoRows {
		return songResolvers, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}

	if start > int32(len(songs)) {
		return songResolvers, nil
	}

	if start+count > int32(len(songs)) {
		count = int32(len(songs)) - start
	}

	songs = songs[start : start+count]

	for _, song := range songs {
		songResolvers = append(songResolvers, NewSongResolver(r.DB, song))
	}

	return songResolvers, nil
}

type InputAddSong struct {
	Name                 string
	RelativeFileLocation string
	FileType             string
	Length               float64
	TrackNumber          int32
	DiscNumber           int32
	Library              graphql.ID
	Artist               graphql.ID
	Album                graphql.ID
	MBID                 *string
}

func (r *Root) AddSong(ctx context.Context, args struct{ Song InputAddSong }) (*SongResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new songs")
	}

	id := uuid.NewV4().String()
	song := args.Song

	_, err := r.DB.Exec(`INSERT INTO Song
    	(id, name, baseType, relativeFileLocation, trackNumber, discNumber, length, fileType, libraryID, createdAt,
    	 albumID, mbID, artist) VALUES (?,?,?,?,?,?,?,?,?,?,?,?, ?)`,
		id, song.Name, enums.BASE_TYPE_SONG, song.RelativeFileLocation, song.TrackNumber, song.DiscNumber, song.Length,
		song.FileType, song.Library, time.Now().Unix(), song.Album, song.MBID, song.Artist)
	if err != nil {
		return nil, err
	}

	addedSong, err := database.GetSongByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewSongResolver(r.DB, addedSong), nil
}

func (r *Root) DeleteSong(args struct{ ID graphql.ID }) (bool, error) {
	_, err := r.DB.Exec("DELETE FROM Song WHERE id=?", args.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}

// Root Album

func (r *Root) Album(args struct{ ID graphql.ID }) (*AlbumResolver, error) {
	album, err := database.GetAlbumByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewAlbumResolver(r.DB, album), nil
}

func (r *Root) Albums(args struct{ Page *Paging }) ([]*AlbumResolver, error) {
	var (
		start          int32 = 0
		count          int32 = DEFAULT_COUNT
		albums               = []*models.Album{}
		albumResolvers       = []*AlbumResolver{}
	)

	err := r.DB.Select(&albums, "SELECT id, name, artist, artURL, createdAt, releaseDate, compilation FROM Albums")

	if err == sql.ErrNoRows {
		return albumResolvers, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}

	if start > int32(len(albums)) {
		return albumResolvers, nil
	}

	if start+count > int32(len(albums)) {
		count = int32(len(albums)) - start
	}

	albums = albums[start : start+count]

	for _, album := range albums {
		albumResolvers = append(albumResolvers, NewAlbumResolver(r.DB, album))
	}

	return albumResolvers, nil
}

type InputAddAlbum struct {
	Name        string
	Compilation *bool
	ArtistID    graphql.ID
	ArtURL      *string
	ReleaseDate *graphql.Time
	Library     graphql.ID
	MBID        *string
}

func (r *Root) AddAlbum(ctx context.Context, args struct{ Album InputAddAlbum }) (*AlbumResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new albums")
	}

	id := uuid.NewV4().String()
	aAlbum := args.Album

	var (
		releaseDate *int64 = nil
		compilation bool   = false
	)

	if aAlbum.ReleaseDate != nil {
		nixtime := args.Album.ReleaseDate.Unix()
		releaseDate = &nixtime
	}

	if aAlbum.Compilation != nil {
		compilation = *aAlbum.Compilation
	}

	_, err := r.DB.Exec("INSERT INTO Albums (id, name, artist, artURL, createdAt, releaseDate, libraryID, mbID, compilation) VALUES (?,?,?,?,?,?,?,?,?)",
		id, aAlbum.Name, string(aAlbum.ArtistID), aAlbum.ArtURL, time.Now().Unix(), releaseDate, aAlbum.Library, aAlbum.MBID, compilation)
	if err != nil {
		return nil, err
	}

	album, err := database.GetAlbumByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewAlbumResolver(r.DB, album), nil
}

func (r *Root) DeleteAlbum(ctx context.Context, args CSCDeleteArgs) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete albums")
	}

	var err error
	if args.Cascade {
		tx := r.DB.MustBegin()
		tx.Exec("DELETE FROM Song WHERE albumID=?", args.ID)
		tx.Exec("DELETE FROM Albums WHERE id=?", args.ID)
		err = tx.Commit()
	} else {
		_, err = r.DB.Exec("DELETE FROM Albums WHERE id=?", args.ID)
	}
	if err != nil {
		return false, err
	}

	return true, nil
}

// Root Artist

type ArtistQuery struct {
	Name *string
	ID   *graphql.ID
}

func (r *Root) Artist(args ArtistQuery) (*ArtistResolver, error) {
	var (
		artist models.Artist
		err    error
	)

	if args.ID != nil {
		err = r.DB.Get(&artist, "SELECT * FROM Artist WHERE id=?", args.ID)
	}
	if args.Name != nil {
		err = r.DB.Get(&artist, "SELECT * FROM Artist WHERE name=?", args.ID)
	}

	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewArtistResolver(r.DB, &artist), nil
}

type InputAddArtist struct {
	Name    string
	Members *[]graphql.ID
	Person  *graphql.ID
	IsGroup bool
}

func (r *Root) AddArtist(ctx context.Context, args struct{ Artist InputAddArtist }) (*ArtistResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new artists")
	}

	id := uuid.NewV4().String()
	tx, _ := r.DB.Begin()

	_, err := tx.Exec("INSERT INTO Artist (id, name, createdAt, person, isGroup) VALUES (?,?,?,?,?)",
		id, args.Artist.Name, time.Now().Unix(), args.Artist.Person, args.Artist.IsGroup)
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	if args.Artist.Members != nil && args.Artist.IsGroup {
		for _, person := range *args.Artist.Members {
			// doing this directly to allow the use of a tx
			_, err := tx.Exec("INSERT INTO artist_person (artist, person) VALUES (?,?)", id, person)
			if err != nil {
				tx.Rollback()
				return nil, err
			}
		}
	}

	tx.Commit()

	var artist models.Artist
	err = r.DB.Get(&artist, "SELECT * FROM Artist WHERE id=?", id)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewArtistResolver(r.DB, &artist), nil
}
