package resolvers

import (
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
)

type Collection interface {
	ID() graphql.ID
	Name() string
	Items() []*MediaItemBaseResolver

	CreatedAt() graphql.Time
}

type CollectionResolver struct {
	Collection
	DB *sqlx.DB
}
