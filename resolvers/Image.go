package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/utils"
	"Server/utils/enums"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

type ImageResolver struct {
	db    *sqlx.DB
	image *models.Image
}

func NewImageResolver(db *sqlx.DB, image *models.Image) *ImageResolver {
	return &ImageResolver{
		db:    db,
		image: image,
	}
}

func (ir *ImageResolver) ID() graphql.ID {
	return ir.image.ID
}

func (ir *ImageResolver) Name() string {
	return ir.image.Name
}

func (ir *ImageResolver) BaseType() string {
	return ir.image.BaseType
}

func (ir *ImageResolver) FileType() string {
	return ir.image.FileType
}

func (ir *ImageResolver) RelativeFileLocation() string {
	return ir.image.RelativeFileLocation
}

func (ir *ImageResolver) Library() (*LibraryResolver, error) {
	library, err := database.GetLibraryByID(ir.db, graphql.ID(ir.image.LibraryID))

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewLibraryResolver(ir.db, library), nil
}

func (ir *ImageResolver) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(ir.image.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}

// Root

func (r *Root) Image(args struct{ ID graphql.ID }) (*ImageResolver, error) {
	image, err := database.GetImageByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return NewImageResolver(r.DB, image), nil
}

func (r *Root) Images(args struct{ Page *Paging }) ([]*ImageResolver, error) {
	var (
		start          int32 = 0
		count          int32 = DEFAULT_COUNT
		images               = []*models.Image{}
		imageResolvers       = []*ImageResolver{}
	)

	err := r.DB.Select(&images, "SELECT * FROM Images")

	if err == sql.ErrNoRows {
		return imageResolvers, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}

	if start > int32(len(images)) {
		return imageResolvers, nil
	}

	if start+count > int32(len(images)) {
		count = int32(len(images)) - start
	}

	images = images[start : start+count]

	for _, image := range images {
		imageResolvers = append(imageResolvers, NewImageResolver(r.DB, image))
	}

	return imageResolvers, nil
}

type AddImage struct {
	Name                 string
	RelativeFileLocation string
	FileType             string
	Library              graphql.ID
}

func (r *Root) AddImage(ctx context.Context, args struct{ Image AddImage }) (*ImageResolver, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new images")
	}

	id := uuid.NewV4().String()

	img := args.Image

	_, err := r.DB.Exec(`INSERT INTO Images (id, name, baseType, relativeFileLocation, fileType, libraryID, createdAt)
    							VALUES (?,?,?,?,?,?,?)`,
		id, img.Name, enums.BASE_TYPE_IMAGE, img.RelativeFileLocation, img.FileType, img.Library, time.Now().Unix())

	if err != nil {
		return nil, err
	}

	image, err := database.GetImageByID(r.DB, graphql.ID(id))
	if err != nil {
		return nil, err
	}

	return NewImageResolver(r.DB, image), nil
}

func (r *Root) DeleteImage(ctx context.Context, args struct{ ID graphql.ID }) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete images")
	}

	_, err := r.DB.Exec("DELETE FROM Images WHERE id=?", args.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}
