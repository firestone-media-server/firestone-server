package resolvers

import (
	"Server/database"
	"Server/models"
	"Server/utils"
	"Server/workers"
	"context"
	"database/sql"
	"errors"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"time"
)

type Root struct {
	DB         *sqlx.DB
	Dispatcher *workers.Wq
}

const (
	DEFAULT_COUNT = 25
)

type Paging struct {
	Start int32
	Count int32
}

// Person

func (r *Root) Person(args struct{ ID graphql.ID }) (*models.Person, error) {
	person, err := database.GetPersonByID(r.DB, args.ID)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return person, nil
}

func (r *Root) People(args struct{ Page *Paging }) ([]*models.Person, error) {
	var (
		people          = []*models.Person{}
		peopleOut       = []*models.Person{}
		start     int32 = 0
		count     int32 = DEFAULT_COUNT
	)

	err := r.DB.Select(&people, "SELECT * FROM Person")

	if err == sql.ErrNoRows {
		return peopleOut, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	if args.Page != nil {
		start = args.Page.Start
		count = args.Page.Count
	}
	if start > int32(len(people)) {
		return peopleOut, nil
	}

	if start+count > int32(len(people)) {
		count = int32(len(people)) - start
	}

	peopleOut = people[start : start+count]

	return peopleOut, nil
}

type InputAddPerson struct {
	Name string
	Born *graphql.Time
	Died *graphql.Time
}

func (r *Root) AddPerson(ctx context.Context, args struct{ Person InputAddPerson }) (*models.Person, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return nil, errors.New("non-admin users cannot add new people")
	}

	id := uuid.NewV4().String()
	var (
		born *int64 = nil
		died *int64 = nil
	)

	if args.Person.Born != nil {
		nixtime := args.Person.Born.Unix()
		born = &nixtime
	}

	if args.Person.Died != nil {
		nixtime := args.Person.Died.Unix()
		died = &nixtime
	}

	_, err := r.DB.Exec("INSERT INTO Person (id, name, born, died, createdAt) VALUES (?,?,?,?,?)",
		id, args.Person.Name, born, died, time.Now().Unix())
	if err != nil {
		return nil, err
	}

	person, err := database.GetPersonByID(r.DB, graphql.ID(id))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return person, nil
}

func (r *Root) DeletePerson(ctx context.Context, args struct{ ID graphql.ID }) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot delete people")
	}

	_, err := r.DB.Exec("DELETE FROM Person WHERE id=?", args.ID)
	if err != nil {
		return false, err
	}

	return true, nil
}

type CSCDeleteArgs struct {
	ID      graphql.ID
	Cascade bool
}

// Links

type LinkPodToPersonArgs struct {
	Podcast graphql.ID
	Person  graphql.ID
}

func (r *Root) LinkPodcastToPerson(ctx context.Context, args LinkPodToPersonArgs) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot link people to podcasts")
	}

	_, err := r.DB.Exec("INSERT INTO podcast_people (podcastID, personID) VALUES (?,?)", args.Podcast, args.Person)
	if err != nil {
		return false, err
	}
	return true, nil
}

type LinkArtistToPersonArgs struct {
	Person graphql.ID
	Artist graphql.ID
}

func (r *Root) LinkArtistToPerson(ctx context.Context, args LinkArtistToPersonArgs) (bool, error) {
	if user := ctx.Value("user"); user == nil || !user.(*utils.User).IsAdmin {
		return false, errors.New("non-admin users cannot link people to artists")
	}

	_, err := r.DB.Exec("INSERT INTO artist_person (artist, person) VALUES (?,?)", args.Artist, args.Person)
	if err != nil {
		return false, err
	}
	return true, nil
}
