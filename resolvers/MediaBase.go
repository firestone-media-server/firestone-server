package resolvers

import (
	"Server/models"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
)

type MediaItemBase interface {
	ID() graphql.ID
	Name() string
	BaseType() string
	RelativeFileLocation() string
	Library() *LibraryResolver
	FileType() string

	CreatedAt() graphql.Time

	// Converters go here ...
	ToSong() (*SongResolver, bool)
	ToImage() (*ImageResolver, bool)
	ToPodcastEpisode() (*PodcastEpisodeResolver, bool)
	ToTVShowEpisode() (*TVShowEpisodeResolver, bool)
	ToHomeVideo() (*HomeVideoResolver, bool)
}

// We just accept that this'll crash with a *nil deref if it can't turn convert
// In other news, @noobayy thinks that a nil pointer dereference is a football (soccer) thing. I don't follow football.
type MediaItemBaseResolver struct {
	MediaItemBase

	ID                   graphql.ID `db:"id"`
	Name                 string
	BaseType             string `db:"baseType"`
	RelativeFileLocation string `db:"relativeFileLocation"`
	FileType             string `db:"fileType"`
	LibraryID            string `db:"libraryID"`

	Length      *float64 `json:"length"`
	TrackNumber *int32   `json:"trackNumber" db:"trackNumber"`
	DiscNumber  *int32   `json:"discNumber" db:"discNumber"`
	AlbumID     *string  `db:"albumID"`
	Artist      graphql.ID
	MBID        *string `db:"mbID"`

	EpisodeNumber *int32     `json:"episodeNumber" db:"episodeNumber"`
	SeasonNumber  *int32     `json:"seasonNumber" db:"seasonNumber"`
	PodcastID     graphql.ID `db:"podcastID"`

	TVShowID string `db:"tvShowID"`
	//RSSFeedURL    *string `json:"rssFeedURL" db:"rssFeedURL"`

	DBReleasedAt *int `db:"releaseDate"`
	DBDateTaken  *int `db:"dateTaken"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`

	DB *sqlx.DB
}

func (r *MediaItemBaseResolver) ToSong() (*SongResolver, bool) {
	song := &models.Song{
		ID:                   r.ID,
		Name:                 r.Name,
		RelativeFileLocation: r.RelativeFileLocation,
		BaseType:             r.BaseType,
		FileType:             r.FileType,
		Length:               *r.Length,
		TrackNumber:          *r.TrackNumber,
		DiscNumber:           *r.DiscNumber,
		LibraryID:            r.LibraryID,
		AlbumID:              *r.AlbumID,
		Artist:               r.Artist,
		DBCreatedAt:          r.DBCreatedAt,
	}

	return NewSongResolver(r.DB, song), true
}

func (r *MediaItemBaseResolver) ToImage() (*ImageResolver, bool) {
	image := &models.Image{
		ID:                   r.ID,
		Name:                 r.Name,
		RelativeFileLocation: r.RelativeFileLocation,
		BaseType:             r.BaseType,
		FileType:             r.FileType,
		LibraryID:            r.LibraryID,

		DBCreatedAt: r.DBCreatedAt,
	}

	return NewImageResolver(r.DB, image), true
}

func (r *MediaItemBaseResolver) ToPodcastEpisode() (*PodcastEpisodeResolver, bool) {
	episode := &models.PodcastEpisode{
		ID:                   r.ID,
		Name:                 r.Name,
		RelativeFileLocation: r.RelativeFileLocation,
		BaseType:             r.BaseType,
		FileType:             r.FileType,
		Length:               *r.Length,
		EpisodeNumber:        *r.EpisodeNumber,
		SeasonNumber:         *r.SeasonNumber,
		DBReleasedAt:         r.DBReleasedAt,
		LibraryID:            r.LibraryID,
		PodcastID:            r.PodcastID,

		DBCreatedAt: r.DBCreatedAt,
	}
	return NewPodcastEpisodeResolver(r.DB, episode), true
}

func (r *MediaItemBaseResolver) ToTVShowEpisode() (*TVShowEpisodeResolver, bool) {
	episode := &models.TVShowEpisode{
		ID:                   r.ID,
		Name:                 r.Name,
		RelativeFileLocation: r.RelativeFileLocation,
		BaseType:             r.BaseType,
		FileType:             r.FileType,
		Length:               *r.Length,
		EpisodeNumber:        *r.EpisodeNumber,
		SeasonNumber:         *r.SeasonNumber,
		LibraryID:            r.LibraryID,
		TVShowID:             r.TVShowID,
		DBReleasedAt:         r.DBReleasedAt,
		DBCreatedAt:          r.DBCreatedAt,
	}
	return NewTVShowEpResolver(r.DB, episode), true
}

func (r *MediaItemBaseResolver) ToHomeVideo() (*HomeVideoResolver, bool) {
	homeVideo := &models.HomeVideo{
		ID:                   r.ID,
		Name:                 r.Name,
		RelativeFileLocation: r.RelativeFileLocation,
		BaseType:             r.BaseType,
		FileType:             r.FileType,
		Length:               *r.Length,
		DBDateTaken:          r.DBDateTaken,
		LibraryID:            r.LibraryID,

		DBCreatedAt: r.DBCreatedAt,
	}
	return NewHomeVideoResolver(r.DB, homeVideo), true
}
