package database

import (
	"Server/models"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
)

func GetAlbumByID(db *sqlx.DB, id graphql.ID) (*models.Album, error) {
	album := models.Album{}

	err := db.Get(&album, "SELECT id, name, artist, artURL, createdAt, releaseDate, compilation FROM Albums WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &album, nil
}

func GetLibraryByID(db *sqlx.DB, id graphql.ID) (*models.Library, error) {
	library := models.Library{}

	err := db.Get(&library, "SELECT * FROM Library WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &library, nil
}

func GetPersonByID(db *sqlx.DB, id graphql.ID) (*models.Person, error) {
	person := models.Person{}

	err := db.Get(&person, "SELECT * FROM Person WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &person, nil
}

func GetSongByID(db *sqlx.DB, id graphql.ID) (*models.Song, error) {
	song := models.Song{}

	err := db.Get(&song, "SELECT id, name, baseType, relativeFileLocation, trackNumber, discNumber, length, fileType, libraryID, albumID, createdAt, artist FROM Song WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &song, nil
}

func GetPodcastEpisodeByID(db *sqlx.DB, id graphql.ID) (*models.PodcastEpisode, error) {
	podcast := models.PodcastEpisode{}

	err := db.Get(&podcast, "SELECT * FROM PodcastEpisode WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &podcast, nil
}

func GetTVShowEpisodeByID(db *sqlx.DB, id graphql.ID) (*models.TVShowEpisode, error) {
	episode := models.TVShowEpisode{}

	err := db.Get(&episode, "SELECT * FROM TVShowEpisode WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &episode, nil
}

func GetImageByID(db *sqlx.DB, id graphql.ID) (*models.Image, error) {
	image := models.Image{}

	err := db.Get(&image, "SELECT * FROM Images WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &image, nil
}

func GetHomeVideoByID(db *sqlx.DB, id graphql.ID) (*models.HomeVideo, error) {
	video := models.HomeVideo{}

	err := db.Get(&video, "SELECT * FROM HomeVideo WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &video, nil
}

func GetPodcastByID(db *sqlx.DB, id graphql.ID) (*models.Podcast, error) {
	podcast := models.Podcast{}

	err := db.Get(&podcast, "SELECT id, name, rssFeedURL, createdAt FROM Podcast WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &podcast, nil
}

func GetTVShowByID(db *sqlx.DB, id graphql.ID) (*models.TVShow, error) {
	tvShow := models.TVShow{}

	err := db.Get(&tvShow, "SELECT id, name, libraryID, createdAt FROM TVShow WHERE id=?", id)

	if err != nil {
		return nil, err
	}

	return &tvShow, nil
}
