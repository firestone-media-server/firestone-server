package utils

type User struct {
	UserID    string `db:"id"`
	Username  string `db:"userName"`
	Password  string `db:"password"`
	IsAdmin   bool   `db:"isAdmin"`
	CreatedAt int    `db:"createdAt"`
}
