package enums

const (
	BASE_TYPE_SONG    = "SONG"
	BASE_TYPE_IMAGE   = "IMAGE"
	BASE_TYPE_PODCAST = "PODCAST"
	BASE_TYPE_TVSHOW  = "TVSHOW"
	BASE_TYPE_HOMEVID = "HOMEVIDEO"
)
