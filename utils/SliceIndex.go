package utils

func SliceIndex(slice []string, want string) int {
	for i, v := range slice {
		if v == want {
			return i
		}
	}
	return -1
}
