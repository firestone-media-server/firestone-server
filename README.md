# Firestone by RPGPN

## How to use it
At the current point of development, we support macOS, Windows and Linux. The code is only tested on MacOS 10.15 Catalina currently, 
however, linux should work. Windows probably won't, at the moment, because of path separators.

Whatever you use, you'll need FFMPEG installed and in the path (sorry avconv users)

Binaries are currently unavailable, so you'll have to build it yourself

## Building
1. Install Go 1.13
2. `git clone` the code
3. `go build` to create a binary

## How do I interact with the server?
An ***experimental*** SwiftUI client exists at rm-firestone/firestone-ios

No other clients exist currently, but you can interact using the GraphQL endpoint at /graphql or using GraphQL playground
which can be found at /playground

## How stable is the project
I have a habit of not stopping the server while I write code, frequently leaving it running for 10s of minutes or
sometimes even hours, before restarting it to test new code - it works pretty well.

## Does it support plugins?
Plugin support is planned - there is no timeframe for this feature, however.