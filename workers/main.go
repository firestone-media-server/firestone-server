package workers

import (
	"context"
	"github.com/graph-gophers/graphql-go"
)

type WorkRequest struct {
	Fn func(context.Context)
}

type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
	GlobalData  *context.Context
}

func NewWorker(id int, workerQueue chan chan WorkRequest, globalData *context.Context) Worker {
	return Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool),
		GlobalData:  globalData,
	}
}

func (w *Worker) Start() {
	go func() {
		for {
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				// Handle work
				work.Fn(*w.GlobalData)
			case <-w.QuitChan:
				return
			}
		}
	}()
}

func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}

type Wq struct {
	WorkQueue  chan WorkRequest
	Schema     *graphql.Schema
	GlobalData context.Context
}

func (w *Wq) Add(fn func(context.Context)) {
	w.WorkQueue <- WorkRequest{
		Fn: fn,
	}
}

func NewWq(ctx context.Context) *Wq {
	wq := &Wq{
		WorkQueue: make(chan WorkRequest, 100),
	}

	wq.GlobalData = context.WithValue(ctx, "workqueue", wq)
	return wq
}

func (w *Wq) SetSchema(schema *graphql.Schema) {
	w.GlobalData = context.WithValue(w.GlobalData, "schema", schema)
}

func (w *Wq) StartDispatcher(n int) {
	workerQueue := make(chan chan WorkRequest, n)

	for i := 0; i < n; i++ {
		worker := NewWorker(i+1, workerQueue, &w.GlobalData)
		worker.Start()
	}

	go func() {
		for {
			select {
			case work := <-w.WorkQueue:
				go func() {
					worker := <-workerQueue

					worker <- work
				}()
			}
		}
	}()
}
