package models

import "github.com/graph-gophers/graphql-go"

type Podcast struct {
	ID          graphql.ID
	Name        string
	RSSFeedURL  *string `json:"rssFeedURL" db:"rssFeedURL"`
	DBCreatedAt int     `json:"createdAt" db:"createdAt"`
}

type PodcastEpisode struct {
	ID                   graphql.ID `json:"id"`
	Name                 string     `json:"name"`
	RelativeFileLocation string     `json:"relativeFileLocation" db:"relativeFileLocation"`
	BaseType             string     `json:"baseType" db:"baseType"`
	FileType             string     `json:"fileType" db:"fileType"`

	Length        float64    `json:"length"`
	EpisodeNumber int32      `json:"episodeNumber" db:"episodeNumber"`
	SeasonNumber  int32      `json:"seasonNumber" db:"seasonNumber"`
	PodcastID     graphql.ID `db:"podcastID"`
	LibraryID     string     `db:"libraryID"`
	DBReleasedAt  *int       `db:"releaseDate"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}
