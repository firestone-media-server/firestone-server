package models

import (
	"github.com/graph-gophers/graphql-go"
)

type Song struct {
	ID                   graphql.ID `json:"id"`
	Name                 string     `json:"name"`
	RelativeFileLocation string     `json:"relativeFileLocation" db:"relativeFileLocation"`
	BaseType             string     `json:"baseType" db:"baseType"`
	FileType             string     `json:"fileType" db:"fileType"`
	Artist               graphql.ID `json:"artist"`

	Length      float64 `json:"length"`
	TrackNumber int32   `json:"trackNumber" db:"trackNumber"`
	DiscNumber  int32   `json:"discNumber" db:"discNumber"`
	LibraryID   string  `db:"libraryID"`
	AlbumID     string  `db:"albumID"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}

type Album struct {
	ID          graphql.ID `json:"id"`
	Name        string     `json:"name"`
	Compilation bool       `json:"compilation"`

	ArtistID    string  `db:"artist"`
	ArtURL      *string `db:"artURL"`
	ReleaseDate *int    `db:"releaseDate"`
	MBID        *string `db:"mbID"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}

type Artist struct {
	ID          graphql.ID `json:"id"`
	Name        string     `json:"name"`
	IsGroup     bool       `json:"isGroup" db:"isGroup"`
	Person      *graphql.ID
	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}
