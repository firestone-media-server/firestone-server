package models

import (
	"github.com/graph-gophers/graphql-go"
	"time"
)

type Person struct {
	ID     graphql.ID `json:"id"`
	Name   string     `json:"name"`
	DBBorn *int       `json:"born" db:"born"`
	DBDied *int       `json:"died" db:"died"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}

func (p *Person) Born() *graphql.Time {
	if p.DBBorn == nil {
		return nil
	}

	born := time.Unix(int64(*p.DBBorn), 0)
	return &graphql.Time{Time: born}
}

func (p *Person) Died() *graphql.Time {
	if p.DBDied == nil {
		return nil
	}

	died := time.Unix(int64(*p.DBDied), 0)
	return &graphql.Time{Time: died}
}

func (p *Person) CreatedAt() graphql.Time {
	createdAtTime := time.Unix(int64(p.DBCreatedAt), 0)
	return graphql.Time{Time: createdAtTime}
}
