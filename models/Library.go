package models

import (
	"github.com/graph-gophers/graphql-go"
)

type Library struct {
	ID       graphql.ID `json:"id"`
	Name     string     `json:"name"`
	Path     string     `json:"path"`
	BaseType string     `json:"baseType" db:"baseMediaType"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}
