package models

import "github.com/graph-gophers/graphql-go"

type Image struct {
	ID                   graphql.ID `json:"id"`
	Name                 string     `json:"name"`
	RelativeFileLocation string     `json:"relativeFileLocation" db:"relativeFileLocation"`
	BaseType             string     `json:"baseType" db:"baseType"`
	FileType             string     `json:"fileType" db:"fileType"`

	LibraryID string `db:"libraryID"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}
