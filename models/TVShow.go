package models

import "github.com/graph-gophers/graphql-go"

type TVShow struct {
	ID          graphql.ID `json:"id"`
	Name        string     `json:"name"`
	LibraryID   string     `db:"libraryID"`
	DBCreatedAt int        `json:"createdAt" db:"createdAt"`
}

type TVShowEpisode struct {
	ID                   graphql.ID `json:"id"`
	Name                 string     `json:"name"`
	RelativeFileLocation string     `json:"relativeFileLocation" db:"relativeFileLocation"`
	BaseType             string     `json:"baseType" db:"baseType"`
	FileType             string     `json:"fileType" db:"fileType"`

	Length        float64 `json:"length"`
	EpisodeNumber int32   `json:"episodeNumber" db:"episodeNumber"`
	SeasonNumber  int32   `json:"seasonNumber" db:"seasonNumber"`

	TVShowID     string `db:"tvShowID"`
	LibraryID    string `db:"libraryID"`
	DBReleasedAt *int   `db:"releaseDate"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}
