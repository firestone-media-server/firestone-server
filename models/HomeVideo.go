package models

import "github.com/graph-gophers/graphql-go"

type HomeVideo struct {
	ID                   graphql.ID `json:"id"`
	Name                 string     `json:"name"`
	RelativeFileLocation string     `json:"relativeFileLocation" db:"relativeFileLocation"`
	BaseType             string     `json:"baseType" db:"baseType"`
	FileType             string     `json:"fileType" db:"fileType"`

	Length    float64 `json:"length"`
	LibraryID string  `db:"libraryID"`

	DBDateTaken *int `db:"dateTaken"`

	DBCreatedAt int `json:"createdAt" db:"createdAt"`
}
