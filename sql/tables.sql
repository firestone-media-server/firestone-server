CREATE TABLE IF NOT EXISTS Library
(
    id            text        not null primary key,
    name          text        not null,
    baseMediaType text        not null,
    path          text unique not null,

    createdAt     integer     not null
);

CREATE TABLE IF NOT EXISTS Person
(
    id        text    not null primary key,
    name      text    not null,
    born      integer,
    died      integer,

    createdAt integer not null
);

CREATE TABLE IF NOT EXISTS Artist
(
    id        text    not null primary key,
    name      text    not null,
    isGroup   boolean not null default false,

    person    text references Person (id),

    createdAt integer not null
);

CREATE TABLE IF NOT EXISTS artist_person
(
    person text not null references Person (id),
    artist text not null references Artist (id)
);

CREATE TABLE IF NOT EXISTS Song
(
    id                   text    not null primary key,
    name                 text    not null,
    baseType             text    not null default 'SONG',
    relativeFileLocation text    not null,
    trackNumber          integer not null default 0,
    discNumber           integer not null default 0,
    length               real    not null default 0.0,
    fileType             text    not null,

    artist               text    not null references Artist (id) on delete cascade,
    libraryID            text    not null references Library (id) on delete cascade,
    albumID              text    not null references Albums (id),

    createdAt            integer not null,
    mbID                 text
);

CREATE TABLE IF NOT EXISTS Albums
(
    id          text    not null primary key,
    name        text    not null,
    compilation boolean not null default false,
    artist      text    not null references Artist (id),
    artURL      text,
    releaseDate integer,
    libraryID   text    not null references Library (id) on delete cascade, -- "Why, we don't use it?": it's for the cascade, to ensure we clean the library out
    createdAt   integer not null,
    mbID        text
);

CREATE TABLE IF NOT EXISTS Podcast
(
    id         text    not null primary key,
    name       text    not null,
    rssFeedURL text,
    libraryID  text    not null references Library (id) on delete cascade,
    createdAt  integer not null
);

CREATE TABLE IF NOT EXISTS podcast_people
(
    podcastID text not null references Podcast (id) on delete cascade,
    personID  text not null references Person (id) on delete cascade
);

CREATE TABLE IF NOT EXISTS Images
(
    id                   text    not null primary key,
    name                 text    not null,
    baseType             text    not null default 'IMAGE',
    relativeFileLocation text    not null,
    fileType             text    not null,

    libraryID            text    not null references Library (id) on delete cascade,
    createdAt            integer not null
);

CREATE TABLE IF NOT EXISTS PodcastEpisode
(
    id                   text    not null primary key,
    name                 text    not null,
    baseType             text    not null default 'PODCAST',
    relativeFileLocation text    not null,
    fileType             text    not null,
    length               real    not null default 0.0,
    episodeNumber        integer not null,
    seasonNumber         integer not null default 1,
    podcastID            text    not null references Podcast (id) on delete cascade,
    releaseDate          integer,

    libraryID            text    not null references Library (id) on delete cascade,
    createdAt            integer not null
);

CREATE TABLE IF NOT EXISTS TVShowEpisode
(
    id                   text    not null primary key,
    name                 text    not null,
    baseType             text    not null default 'TVSHOW',
    relativeFileLocation text    not null,
    fileType             text    not null,
    length               real    not null default 0.0,
    episodeNumber        integer not null,
    seasonNumber         integer not null default 1,
    releaseDate          integer,
    tvShowID             text references TVShow (id) on delete cascade,

    libraryID            text    not null references Library (id) on delete cascade,
    createdAt            integer not null
);

CREATE TABLE IF NOT EXISTS TVShow
(
    id        text    not null primary key,
    name      text    not null,
    libraryID text    not null references Library (id) on delete cascade,
    createdAt integer not null
);

CREATE TABLE IF NOT EXISTS HomeVideo
(
    id                   text    not null primary key,
    name                 text    not null,
    baseType             text    not null default 'HOMEVIDEO',
    relativeFileLocation text    not null,
    fileType             text    not null,
    length               real    not null default 0.0,
    dateTaken            integer,

    libraryID            text    not null references Library (id) on delete cascade,
    createdAt            integer not null
);

-- Users and Permissions

CREATE TABLE IF NOT EXISTS User
(
    id        text    not null primary key, --also uuid
    userName  text    not null,
    password  text    not null,
    isAdmin   boolean not null default false,

    createdAt integer not null
);
