package main

import (
	"Server/resolvers"
	"Server/streaming"
	"Server/workers"
	"context"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"
	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/michiwend/gomusicbrainz"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"os"
)

const (
	MBZ_WS_URL = "https://musicbrainz.org/ws/2"
	// 21 days
	SESSION_TIME = 1 * 60 * 60 * 24 * 21
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)

	db, err := sqlx.Connect("sqlite3", "firestone_"+os.Getenv("BRANCH")+".db?_fk=ON")
	if err != nil {
		logrus.Panic(err)
	}

	defer db.Close()

	// Musicbrainz client
	mbzClient, err := gomusicbrainz.NewWS2Client(MBZ_WS_URL, "RPGPN Firestone <IN DEV>", "0.0.2-beta", "ponkey364 (at) outlook (dot) com")
	if err != nil {
		logrus.Panic(err)
	}

	// JWT Auth
	tokenAuth := jwtauth.New("HS256", []byte(os.Getenv("SESH")), nil)

	// Chi router
	router := chi.NewRouter()

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.WithValue("tokenAuth", tokenAuth))
	router.Use(middleware.WithValue("db", db))

	router.Use(jwtauth.Verifier(tokenAuth))

	// Set up the context for the dispatcher
	dispCtx := context.WithValue(nil, "mbz_client", mbzClient)
	dispCtx = context.WithValue(dispCtx, "db", db)

	dispatcher := workers.NewWq(dispCtx)

	stringSchema, err := loadFileToString("./schema.graphql")
	if err != nil {
		logrus.Panic(err)
	}

	schema := graphql.MustParseSchema(stringSchema, &resolvers.Root{DB: db, Dispatcher: dispatcher}, graphql.UseStringDescriptions(), graphql.UseFieldResolvers())

	dispatcher.SetSchema(schema)
	dispatcher.StartDispatcher(8)

	// Load the routes up
	router.With(SessionMiddleware(true)).Handle("/graphql", &relay.Handler{Schema: schema})

	router.Mount("/auth", SetupAuthnRouter())
	router.With(SessionMiddleware(true)).Mount("/media", streaming.Register(db))

	playground, err := ioutil.ReadFile("./playground.html")
	if err != nil {
		logrus.Panic(err)
	}

	router.HandleFunc("/playground", func(w http.ResponseWriter, r *http.Request) {
		w.Write(playground)
	})

	var port string // I know.
	if p := os.Getenv("PORT"); p != "" {
		port = p
	} else {
		port = "8080"
	}

	http.ListenAndServe(":"+port, router)
}

func loadFileToString(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}
