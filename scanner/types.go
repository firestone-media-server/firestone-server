package scanner

type TVShowEpisode struct {
	EpisodeNumber        int
	SeasonNumber         int
	ShowName             string
	FileExt              string
	Title                string
	RelativeFileLocation string
}

type TVShow struct {
	Name  string
	Items []*TVShowEpisode
}

func (ts *TVShow) AppendItem(item *TVShowEpisode) {
	ts.Items = append(ts.Items, item)
}

type HomeVideo struct {
	Name                 string
	FileExt              string
	RelativeFileLocation string
}

type Song struct {
	Name                 string
	FileExt              string
	RelativeFileLocation string
	Length               float64
	TrackNumber          int
	DiscNumber           int
	AlbumName            string
	AlbumMBID            *string
	MBID                 *string
	Artist               string
	AlbumArtist          string
}

type Artist struct {
	Name   string
	Albums map[string]*Album
}

type Album struct {
	Name  string
	MBID  *string
	Items []*Song
}

func (a *Album) AppendItem(item *Song) {
	a.Items = append(a.Items, item)
}

type Image struct {
	Name                 string
	FileExt              string
	RelativeFileLocation string
}
