package scanner

const AddTVShowEpisode = `
mutation AddTVShowEpisode(
  $name: String!
  $fileType: String!
  $episodeNumber: Int!
  $seasonNumber: Int!
  $relativeFileLocation: String!
  $library: ID!
  $tvShow: ID!
  $length: Float!
) {
  addTVShowEpisode(
    episode: {
      name: $name
      fileType: $fileType
      episodeNumber: $episodeNumber
      seasonNumber: $seasonNumber
      relativeFileLocation: $relativeFileLocation
      library: $library
      length: $length
      tvShow: $tvShow
    }
  ) {
    id
  }
}`

const AddTVShow = `
mutation AddTVShow (
  $name: String!
  $library: ID!
) {
  addTVShow(show: {
	name: $name
	library: $library
  }) {
	id
  }
}`

const AddHomeVideo = `
mutation AddHomeVideo(
  $name: String!
  $fileType: String!
  $library: ID!
  $relativeFileLocation:String!
  $length: Float!
){
  addHomeVideo(video: {
    name: $name
    fileType: $fileType
    library: $library
    relativeFileLocation: $relativeFileLocation
    length:$length
  }){
    id
  }
}`

const AddSong = `
mutation AddSong(
  $album: ID!
  $library: ID!
  $name: String!
  $trackNumber: Int!
  $discNumber: Int!
  $length: Float!
  $fileType: String!
  $relativeFileLocation: String!
  $mbID: String
  $artist: ID!
) {
  addSong(
    song: {
      album: $album
      library: $library
      name: $name
      trackNumber: $trackNumber
      fileType: $fileType
      length: $length
      discNumber: $discNumber
      relativeFileLocation: $relativeFileLocation
	  mbID: $mbID
	  artist: $artist
    }
  ) {
    id
  }
}`

const AddAlbum = `
mutation AddAlbum(
  $name: String!
  $artist: ID!
  $library: ID!
  $mbID: String
  $comp: Boolean!
) {
  addAlbum(
    album: {
      name: $name
      artistID: $artist
      library: $library
      mbID: $mbID
      compilation: $comp
    }
  ) {
    id
  }
}
`

const AddImage = `
mutation AddImage(
  $name: String!
  $fileType: String!
  $library: ID!
  $relativeFileLocation: String!
) {
  addImage(
    image: {
      name: $name
      fileType: $fileType
      library: $library
      relativeFileLocation: $relativeFileLocation
    }
  ) {
    id
  }
}`

const AddArtist = `
mutation AddArtist($name:String!) {
  addArtist (artist: {
    name: $name
    isGroup: false # we'll find out later.
  }) {
    id
  }
}
`
