package scanner

func GroupTVShows(episodes []interface{}) map[string]*TVShow {
	shows := make(map[string]*TVShow)

	for _, episode := range episodes {
		ep := episode.(*TVShowEpisode)
		if show, ok := shows[ep.ShowName]; ok {
			show.AppendItem(ep)
			continue
		} else {
			shows[ep.ShowName] = &TVShow{
				Name: ep.ShowName,
			}
			shows[ep.ShowName].AppendItem(ep)
		}
	}

	return shows
}

func GroupSongs(songs []interface{}) map[string]*Artist {
	artists := make(map[string]*Artist)

	for _, song := range songs {
		s := song.(*Song)

		if s == nil {
			continue
		}

		var albumArtist string
		if s.AlbumArtist == "" && s.Artist == "" {
			albumArtist = UNKNOWN_PERSON_ID
		} else if s.AlbumArtist == "" {
			albumArtist = s.Artist
		} else {
			albumArtist = s.AlbumArtist
		}

		if artist, ok := artists[albumArtist]; ok {
			if album, ok := artist.Albums[s.AlbumName]; ok {
				album.AppendItem(s)
				continue
			}
			artist.Albums[s.AlbumName] = &Album{
				Name: s.AlbumName,
				MBID: s.AlbumMBID,
			}
			artist.Albums[s.AlbumName].AppendItem(s)

			continue
		}

		artists[albumArtist] = &Artist{
			Name:   albumArtist,
			Albums: make(map[string]*Album),
		}

		artists[albumArtist].Albums[s.AlbumName] = &Album{
			Name: s.AlbumName,
			MBID: s.AlbumMBID,
		}
		artists[albumArtist].Albums[s.AlbumName].AppendItem(s)
	}

	return artists
}
