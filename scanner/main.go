package scanner

import (
	"Server/metadata"
	"Server/utils"
	"Server/utils/enums"
	"Server/workers"
	"context"
	"database/sql"
	"encoding/json"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strings"
)

const UNKNOWN_PERSON_ID = "UNKNOWN"

// Felt slicey, might radix later.
var extsVideo = []string{".MP4", ".MOV", ".TS"}
var extsAudio = []string{".M4A", ".MP3", ".FLAC", ".WAV"}
var extsImage = []string{".CR2", ".JPG", ".PNG", ".JPEG"}
var systemUser = &utils.User{
	Username: "system",
	UserID:   "system",
	IsAdmin:  true,
}

func ScanDir(data map[string]string, ctx context.Context) {
	schema := ctx.Value("schema").(*graphql.Schema)
	path := data["path"]

	logrus.Trace("Scanning", path)

	var files []string

	err := filepath.Walk(path, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}

		ext := strings.ToUpper(filepath.Ext(filePath)) // It's just easier to always work in CAPS

		// We don't want DS_Stores.
		if ext == ".DS_STORE" {
			return nil
		}

		// Filter down to just what we want so we don't have to later.
		switch data["baseType"] {
		case enums.BASE_TYPE_HOMEVID:
		case enums.BASE_TYPE_TVSHOW:
			if utils.SliceIndex(extsVideo, ext) == -1 {
				return nil
			}
		case enums.BASE_TYPE_SONG:
			if utils.SliceIndex(extsAudio, ext) == -1 {
				return nil
			}
		case enums.BASE_TYPE_IMAGE:
			if utils.SliceIndex(extsImage, ext) == -1 {
				return nil
			}
		}

		files = append(files, filePath)
		return nil
	})
	if err != nil {
		logrus.Error(err)
		return
	}

	var scanned []interface{}
	for _, file := range files {
		f, err := ScanFile(file, path, data["baseType"])
		if err != nil {
			logrus.Error(err)
			continue
		}
		scanned = append(scanned, f)
	}

	switch data["baseType"] {
	case enums.BASE_TYPE_TVSHOW:
		{
			grouped := GroupTVShows(scanned)

			for _, tvShow := range grouped {
				id, err := CreateTVShow(tvShow, data["id"], schema)
				if err != nil {
					logrus.Error(err)
					continue
				}

				for _, episode := range tvShow.Items {
					err = CreateTVShowEpisode(data["id"], *id, schema, episode)
					if err != nil {
						logrus.Error(err)
						continue
					}
				}
			}
		}
	case enums.BASE_TYPE_HOMEVID:
		{
			for _, v := range scanned {
				err := CreateHomeVideo(data["id"], schema, v.(*HomeVideo))
				if err != nil {
					logrus.Error(err)
					continue
				}
			}
		}
	case enums.BASE_TYPE_SONG:
		{
			grouped := GroupSongs(scanned)

			for _, artist := range grouped {
				var artistID *string
				if artist.Name == UNKNOWN_PERSON_ID {
					unknown := UNKNOWN_PERSON_ID
					artistID = &unknown
				} else {
					// Before we try creating an artist, try checking the database (this is a problem that appears once in a
					// while because of compilation albums). Plus we have direct access to the db now, so it's nice and easy
					var id string
					err = ctx.Value("db").(*sqlx.DB).Get(&id, "SELECT id FROM Artist WHERE name=?", artist.Name)
					if err == nil {
						logrus.Debug(id, artist)
						artistID = &id
					} else {
						logrus.Warn(err)
						artistID, err = CreateArtist(schema, artist)
						if err != nil {
							logrus.Error(err)
							continue
						}
					}
				}

				for _, album := range artist.Albums {
					comp := false
					if artist.Name == "Various Artists" {
						comp = true
					}

					albumID, err := CreateAlbum(data["id"], schema, album, *artistID, comp)
					if err != nil {
						logrus.Error(err)
						continue
					}

					for _, song := range album.Items {
						var trackArtistID string
						if song.Artist == song.AlbumArtist || song.Artist == "" || song.AlbumArtist == "" {
							trackArtistID = *artistID
						} else {
							var id string
							err = ctx.Value("db").(*sqlx.DB).Get(&id, "SELECT id FROM Artist WHERE name=?", song.Artist)
							if err != nil {
								if err != sql.ErrNoRows {
									logrus.Error(err)
								}
								createdID, err := CreateArtist(schema, &Artist{Name: song.Artist})
								if err != nil {
									logrus.Error(err)
									continue
								}
								trackArtistID = *createdID
							} else {
								trackArtistID = id
							}

						}

						err = CreateSong(data["id"], *albumID, schema, song, trackArtistID)
						if err != nil {
							logrus.Error(err)
							continue
						}
					}
				}
			}
			ctx.Value("workqueue").(*workers.Wq).Add(func(ctx context.Context) {
				metadata.MBZHandleLibrary(ctx, data["id"])
			})
		}
	case enums.BASE_TYPE_IMAGE:
		{
			for _, image := range scanned {
				err := CreateImage(data["id"], schema, image.(*Image))
				if err != nil {
					logrus.Error(err)
					continue
				}
			}
		}
	}
}

// Takes a path and then the rootDir, so we can do some magic like detecting names from the paths.
func ScanFile(path string, rootDir string, baseType string) (interface{}, error) {
	relativeLocation := path[len(rootDir)+1:] // make sure we +1 to remove the /

	splitPath := strings.Split(relativeLocation, string(os.PathSeparator))

	switch baseType {
	case enums.BASE_TYPE_TVSHOW:
		// The expected format is [Show name]/(Season|Series) [s]/S[s} E[_e] [name].[container]
		showName := splitPath[0]
		return ScanTVShow(relativeLocation, showName)
	case enums.BASE_TYPE_HOMEVID:
		return ScanHomeVideo(relativeLocation)
	case enums.BASE_TYPE_SONG:
		return ScanSong(relativeLocation, rootDir)
	case enums.BASE_TYPE_IMAGE:
		return ScanImage(relativeLocation), nil
	}

	return nil, nil
}

func CreateTVShow(show *TVShow, library string, schema *graphql.Schema) (*string, error) {
	vars := make(map[string]interface{})
	vars["name"] = show.Name
	vars["library"] = library

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddTVShow,
		"AddTVShow", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	var data struct {
		AddTVShow struct {
			ID string
		} `json:"addTVShow"`
	}

	err := json.Unmarshal(res.Data, &data)
	if err != nil {
		return nil, err
	}

	return &data.AddTVShow.ID, nil
}

func CreateTVShowEpisode(libraryID string, tvShowID string, schema *graphql.Schema, episode *TVShowEpisode) error {
	vars := make(map[string]interface{})
	vars["name"] = episode.Title
	vars["fileType"] = episode.FileExt // For now
	vars["episodeNumber"] = episode.EpisodeNumber
	vars["seasonNumber"] = episode.SeasonNumber
	vars["relativeFileLocation"] = episode.RelativeFileLocation
	vars["library"] = libraryID
	vars["length"] = 0.0
	vars["tvShow"] = tvShowID

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddTVShowEpisode, "AddTVShowEpisode", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	return nil
}

func CreateHomeVideo(libraryID string, schema *graphql.Schema, video *HomeVideo) error {
	vars := make(map[string]interface{})
	vars["name"] = video.Name
	vars["fileType"] = video.FileExt
	vars["library"] = libraryID
	vars["relativeFileLocation"] = video.RelativeFileLocation
	vars["length"] = 0.0

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddHomeVideo, "AddHomeVideo", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	return nil
}

func CreateAlbum(libraryID string, schema *graphql.Schema, album *Album, artistID string, comp bool) (*string, error) {
	vars := make(map[string]interface{})
	vars["name"] = album.Name
	vars["artist"] = artistID
	vars["library"] = libraryID
	vars["comp"] = comp
	if album.MBID != nil {
		vars["mbID"] = *album.MBID
	}

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddAlbum, "AddAlbum", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	var data struct {
		AddAlbum struct {
			ID string
		} `json:"addAlbum"`
	}

	err := json.Unmarshal(res.Data, &data)
	if err != nil {
		return nil, err
	}

	return &data.AddAlbum.ID, nil
}

func CreateArtist(schema *graphql.Schema, artist *Artist) (*string, error) {
	vars := make(map[string]interface{})
	vars["name"] = artist.Name

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddArtist, "AddArtist", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	var data struct {
		AddArtist struct {
			ID string
		} `json:"addArtist"`
	}

	err := json.Unmarshal(res.Data, &data)
	if err != nil {
		return nil, err
	}

	return &data.AddArtist.ID, nil
}

func CreateSong(libraryID string, album string, schema *graphql.Schema, song *Song, artistID string) error {
	vars := make(map[string]interface{})
	vars["name"] = song.Name
	vars["fileType"] = song.FileExt // For now
	vars["trackNumber"] = song.TrackNumber
	vars["discNumber"] = song.DiscNumber
	vars["relativeFileLocation"] = song.RelativeFileLocation
	vars["library"] = libraryID
	vars["length"] = song.Length
	vars["album"] = album
	vars["artist"] = artistID
	if song.MBID != nil {
		vars["mbID"] = *song.MBID
	}

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddSong, "AddSong", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	return nil
}

func CreateImage(libraryID string, schema *graphql.Schema, image *Image) error {
	vars := make(map[string]interface{})
	vars["name"] = image.Name
	vars["fileType"] = image.FileExt
	vars["library"] = libraryID
	vars["relativeFileLocation"] = image.RelativeFileLocation

	ctx := context.WithValue(context.Background(), "user", systemUser)

	res := schema.Exec(ctx, AddImage, "AddImage", vars)

	for _, err := range res.Errors {
		if err != nil {
			logrus.Error(err)
		}
	}

	return nil
}
