package scanner

import (
	"Server/metadata"
	"github.com/dhowden/tag"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
)

const (
	//First, Match Season or Series, because the two get used interchangeably, then match whitespace, then group a number
	//This number is the season number - then waltz past the / or \ (what fun, 4 \, damn you windows) then find the season
	//number again and ignore it, the next thing we find is a space, then the E and group the episode number. NEXT - grab
	//a space and then group what's probably the name - but we can check that against an external DB. Then a dot, then
	//group the file extension declare that this should be the end of the string
	BFR_TVSHOW = "(?:Season|Series)\\s*(\\d*)(?:\\/|\\\\)S\\d*\\sE(\\d*)(?:\\s|\\s\\-\\s)(.*)\\.(.*)$"
	// Look, it works, okay? It's nicer if you ignore the double escape sequences.
	BFR_HOMEVID = "(.*)(?:\\\\|\\/)(.*)\\.(.*)$"

	BFR_SONG = "^(.*)(?:\\\\|\\/)(.*)(?:\\\\|\\/)(\\d*)(?:\\s-\\s|\\s)(.*)\\.(.*)$"

	BFR_IMAGE = "([^\\/]*)\\.(.*)$"
)

func ScanTVShow(relativeLocation string, showName string) (*TVShowEpisode, error) {
	defer deferRecovery(relativeLocation)

	r := regexp.MustCompile(BFR_TVSHOW)
	//Groups: season#, episode#, ep name, ext
	st := r.FindStringSubmatch(relativeLocation)

	eNumber, err := strconv.Atoi(st[2])
	if err != nil {
		return nil, err
	}

	sNumber, err := strconv.Atoi(st[1])
	if err != nil {
		return nil, err
	}

	return &TVShowEpisode{
		EpisodeNumber:        eNumber,
		SeasonNumber:         sNumber,
		ShowName:             showName,
		FileExt:              st[4],
		Title:                st[3],
		RelativeFileLocation: relativeLocation,
	}, nil
}

func ScanHomeVideo(relativeLocation string) (*HomeVideo, error) {
	defer deferRecovery(relativeLocation)

	r := regexp.MustCompile(BFR_HOMEVID)
	st := r.FindStringSubmatch(relativeLocation)

	return &HomeVideo{
		Name:                 st[2],
		FileExt:              st[3],
		RelativeFileLocation: relativeLocation,
	}, nil
}

func ScanSong(relativeLocation string, rootDir string) (*Song, error) {
	defer deferRecovery(relativeLocation)

	// First, do some FFMPEG magic to get the length of the file, in seconds.
	length, err := metadata.GetFileLength(rootDir + "/" + relativeLocation)
	if err != nil {
		return nil, err
	}

	// Before we even bother with regex, ID3 should be adequate
	data, err := metadata.GetSongMeta(rootDir + "/" + relativeLocation)

	if err == tag.ErrNoTagsFound {
		logrus.Warn("Falling back to regex for ", relativeLocation)
		// Use regex
		r := regexp.MustCompile(BFR_SONG)
		st := r.FindStringSubmatch(relativeLocation)

		trackNumber, err := strconv.Atoi(st[3])
		if err != nil {
			return nil, err
		}

		return &Song{
			Name:                 st[4],
			FileExt:              st[5],
			RelativeFileLocation: relativeLocation,
			TrackNumber:          trackNumber,
			AlbumName:            st[2],
			AlbumArtist:          st[1],
			Length:               *length,
		}, nil
	}

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	track, _ := (*data).Track()
	disc, _ := (*data).Disc()
	raw := (*data).Raw()

	var (
		album_id *string = nil
		track_id *string = nil
	)

	if id, ok := raw["musicbrainz_albumid"]; ok {
		aid := id.(string)
		album_id = &aid
	}

	if id, ok := raw["musicbrainz_trackid"]; ok {
		tid := id.(string)
		track_id = &tid
	}

	return &Song{
		Name:                 (*data).Title(),
		FileExt:              string((*data).FileType()),
		RelativeFileLocation: relativeLocation,
		TrackNumber:          track,
		AlbumName:            (*data).Album(),
		DiscNumber:           disc,
		AlbumMBID:            album_id,
		MBID:                 track_id,
		Artist:               (*data).Artist(),
		AlbumArtist:          (*data).AlbumArtist(),
		Length:               *length,
	}, nil
}

func ScanImage(relativeLocation string) *Image {
	defer deferRecovery(relativeLocation)

	r := regexp.MustCompile(BFR_IMAGE)
	st := r.FindStringSubmatch(relativeLocation)
	return &Image{
		Name:                 st[1],
		FileExt:              st[2],
		RelativeFileLocation: relativeLocation,
	}
}

func deferRecovery(relativeLocation string) {
	r := recover()
	if r != nil {
		logrus.Warn("Skipping ", relativeLocation, " because: ", r)
	}
}
