package metadata

import (
	"Server/models"
	"context"
	"database/sql"
	"fmt"
	"github.com/graph-gophers/graphql-go"
	"github.com/jmoiron/sqlx"
	"github.com/michiwend/gomusicbrainz"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

const CAA_BASE = "https://coverartarchive.org/release/"

func MBZHandleLibrary(ctx context.Context, library string) {
	db := ctx.Value("db").(*sqlx.DB)
	mbz := ctx.Value("mbz_client").(*gomusicbrainz.WS2Client)
	err := _MBZHandleAlbums(db, mbz, library)
	if err != nil {
		logrus.Error(err)
		return
	}
}

func _MBZHandleAlbums(db *sqlx.DB, mbz *gomusicbrainz.WS2Client, library string) error {
	// do everything with a mbid first
	var albums []*models.Album
	err := db.Select(&albums, `SELECT id, name, artist, artURL, createdAt, releaseDate, compilation, mbID
									FROM Albums
									WHERE mbID not null AND releaseDate is null AND libraryID=?`, library)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	for _, DBAlbum := range albums {
		release, err := mbz.LookupRelease(gomusicbrainz.MBID(*DBAlbum.MBID))
		if err != nil {
			return err
		}

		_, err = db.Exec("UPDATE Albums SET releaseDate=? WHERE id=?", release.Date.Unix(), DBAlbum.ID)
		if err != nil {
			return err
		}

		time.Sleep(time.Second)
	}

	// now stuff without an MBID
	var nAlbums []*NoMBAlbum
	err = db.Select(&nAlbums, `SELECT Albums.id as AlID, Albums.name as AlName, artURL, releaseDate, A.name AS ArName
									FROM Albums
									JOIN Artist A on Albums.artist = A.id
									WHERE mbID is null AND releaseDate is null AND libraryID=?`, library)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	for _, DBAlbum := range nAlbums {
		// We should check here is the album contains "- Single" because mb can have issues with that sometimes.
		// Also has issues with (Deluxe), but we sorta need that.
		resp, err := mbz.SearchRelease(fmt.Sprintf("%s AND artist:%s", DBAlbum.AlbumName, DBAlbum.ArtistName), 5, 0)
		if err != nil {
			return err
		}

		top := resp.ResultsWithScore(95)

		if len(top) == 0 {
			logrus.Warn("No good info found for ", DBAlbum.AlbumName)
			continue
		}
		chosen := top[0]

		_, err = db.Exec("UPDATE Albums SET releaseDate=?, mbID=? WHERE id=?", chosen.Date.Unix(), chosen.ID, DBAlbum.ID)
		if err != nil {
			return err
		}

		time.Sleep(time.Second)
	}

	// Now handle album art
	var artAlbums []*models.Album
	err = db.Select(&artAlbums, `SELECT id, name, artURL, releaseDate, mbID
									FROM Albums
									WHERE mbID not null AND artURL is null AND libraryID=?`, library)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	client := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	for _, DBAlbum := range artAlbums {
		time.Sleep(time.Second / 2)
		res, err := client.Get(CAA_BASE + *DBAlbum.MBID + "/front")
		if err != nil {
			return err
		}
		switch res.StatusCode {
		case 307:
			loc, err := res.Location()
			if err != nil {
				return err
			}

			_, err = db.Exec("UPDATE Albums SET artURL=? WHERE id=?", loc.String(), DBAlbum.ID)
			if err != nil {
				return err
			}
		case 400:
			logrus.Warn("Invalid UUID ", DBAlbum.MBID)
			continue
		case 404:
			logrus.Warn("No cover art found for ", DBAlbum.Name)
			continue
		case 503:
			logrus.Error("Rate limit exceeded")
			continue
		}
	}

	return nil
}

type NoMBAlbum struct {
	ID         graphql.ID `json:"id" db:"AlID"`
	AlbumName  string     `json:"name" db:"AlName"`
	ArtistName string     `json:"name" db:"ArName"`

	ArtURL      *string `db:"artURL"`
	ReleaseDate *int    `db:"releaseDate"`
	MBID        *string `db:"mbID"`
}
