package metadata

import (
	"encoding/json"
	"github.com/dhowden/tag"
	"os"
	"os/exec"
	"time"
)

const FFPROBE_DIR = "ffprobe"

func GetSongMeta(filePath string) (*tag.Metadata, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	meta, err := tag.ReadFrom(file)
	if err != nil {
		return nil, err
	}

	return &meta, nil
}

type FFProbeOutput struct {
	Format *struct {
		Duration string
	}
}

func getFileLengthRaw(path string) (*time.Duration, error) {
	var cmdLine = []string{
		"-v", "quiet",
		"-print_format", "json",
		"-show_format",
		path,
	}

	cmd := exec.Command(FFPROBE_DIR, cmdLine...)

	bOutput, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	var ffpo *FFProbeOutput

	json.Unmarshal(bOutput, &ffpo)

	t, err := time.ParseDuration(ffpo.Format.Duration + "s")
	if err != nil {
		return nil, err
	}

	return &t, err
}

func GetFileLengthUnrounded(path string) (*float64, error) {
	t, err := getFileLengthRaw(path)
	if err != nil {
		return nil, err
	}

	seconds := t.Seconds()
	return &seconds, nil
}

func GetFileLength(path string) (*float64, error) {
	t, err := getFileLengthRaw(path)
	if err != nil {
		return nil, err
	}

	seconds := t.Round(time.Second).Seconds()
	return &seconds, nil
}
